## KVM Preseed

Here's the preseed file I use to modify Debian isos for my Proxmox VMs.

I download the latest iso, rename it to debian.iso then run my [preseed2iso](https://gitlab.com/preseed/preseed-scripts) script to create the new iso with the preseed inside.

Get the script: https://gitlab.com/preseed/preseed-scripts 

Or you can host it with python: `python3 -m http.server`

After installing using this, I like to run my [kvm ansible playbooks](https://gitlab.com/ansible9/ansible-kvm) to finish setting up the vm.

Currently used on Debian Buster 10.4

